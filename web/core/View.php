<?php

namespace Core;

class View {

    public static function render($viewName, array $data = array()) {
        $fileToRender = TEMPLATE_DIR . $viewName . '.php';
        //echo file_get_contents($fileToRender);
    
        if (!file_exists($fileToRender)) {
          return "Error loading template file ($fileToRender).";
        }

        ob_start();
        include($fileToRender);
        $output = ob_get_contents();
        ob_end_clean();
    
//        $output = file_get_contents($fileToRender);
//        foreach ($data as $key => $value) {
//          $tagToReplace = "{@$key}";
//          $output = str_replace($tagToReplace, $value, $output);
//        }
        echo $output;
      }
}