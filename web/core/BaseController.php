<?php

namespace Core;

class BaseController {

  public function jsonResponse($data, $responseCode) {
    header('Content-Type: application/json');
    http_response_code($responseCode);
    echo json_encode($data);
  }

}