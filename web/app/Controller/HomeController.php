<?php

namespace App\Controller;

use App\Model\Currency;
use Core\BaseController;
use Core\Request;
use Core\View;

class HomeController extends BaseController
{
    public function index() {
        $request = new Request();
        $currencyCodes = $request->send('GET', 'http://localhost:8888/currencies/codes');
//        var_dump(json_decode($currencyCodes, true)); die;
        View::render('home', json_decode($currencyCodes, true));
    }
}