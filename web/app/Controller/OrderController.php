<?php

namespace App\Controller;

use App\Model\Order;
use Core\BaseController;
use Core\View;

class OrderController extends BaseController {

  public function calculate() {
//     var_dump($_POST); die;
    $currency = $_POST['currency'];
    $amount = $_POST['amount'];
    $calculated = Order::preCalculate($currency, $amount);
    View::render('calculated', [
        'currency' => $currency,
        'amount' => $amount,
        'calculatedAmount' => $calculated[0],
        'surcharge' => $calculated[1],
        'total' => $calculated[2]
        ]);
  }
}