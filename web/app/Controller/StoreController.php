<?php

namespace App\Controller;

use App\Model\Store;
use Core\BaseController;
use Core\View;

class StoreController extends BaseController {
    public function store($request) {
        Store::store($_POST);
        $storeSuccess = false;
        if ($storeSuccess) {
            View::render('succes');
        } else {
            View::render('fail');
        }
    }
}
