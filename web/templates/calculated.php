<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="container">
    <form action="/store" method="post">
        <div class="container">
            <h1>Confirmation page</h1>
            <p>Please check if provided data is correct...</p>
            <hr>
            <div id="info">
                <p>Amount of {@currency} to purchase is: <span class="bold">{@amount}</span></p>
                <hr>
                <h3>Transaction informations</h3>
                <hr>
                <p>Value in American Dollars: <span class="bold">{@calculatedAmount}</span></p>
                <p>Surcharge: <span class="bold">{@surcharge}</span></p>
                <p>Total: <span class="bold">{@total}</span></p>
                <input
                    class="hidden"
                    type="text"
                    name="currency"
                    value="{@currency}"
                >
                <br>
                <input
                    class="hidden"
                    type="text"
                    name="amount"
                    value="{@amount}"
                >
                <br>
                <input
                    class="hidden"
                    type="text"
                    name="calculated_amount"
                    value="{@calculatedAmount}"
                >
                <br>
                <input
                    class="hidden"
                    type="text"
                    name="surcharge"
                    value="{@surcharge}"
                >

                <br>
                <input
                    class="hidden"
                    type="text"
                    name="total"
                    value="{@total}"
                >
                <br>
            </div>

            <button type="submit" class="submitbtn">Submit</button>
        </div>
    </form>
</div>
</body>
</html>