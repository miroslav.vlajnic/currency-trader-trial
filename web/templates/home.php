<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<form action="/confirm" method="post">
    <div class="container">
        <h1>Welcome</h1>
        <p>Please fill in this form...</p>
        <hr>
        <?php var_dump(get_defined_vars()); die; ?>
        <label for="currency"><b>Please select currency/country</b></label>
        <select name="currency" id="currency">

        <?php foreach ($data[0] as $currency) {?>

            <option value="jpy"><?php echo $currency['currency_code'] ?></option>
            <?php } ?>
        </select>

        <label for="amount"><b>Amount</b></label>
        <input id="amount" type="text" placeholder="Please enter amount of currency..." name="amount" required>
        <hr>
        <button id="calculate" type="submit">Calculate</button>
    </div>
</form>
<!--    <script src="script.js"></script>-->
</body>
</html>