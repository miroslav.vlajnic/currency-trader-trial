<?php

namespace App\Model;

use Core\BaseModel;

class Currency extends BaseModel {

  public function getAll() {

    $stmt = $this->db->query('SELECT * FROM currency');
    return $stmt->fetchAll(\PDO::FETCH_ASSOC);
//      var_dump($stmt->fetchAll(\PDO::FETCH_ASSOC));
  }

  public function getCurrencyCodes () {
      $stmt = $this->db->query('SELECT currency_code FROM currency');
      return $stmt->fetchAll(\PDO::FETCH_ASSOC);
  }

}

//$stmt = $pdo->prepare("DELETE FROM goods WHERE category = ?");
//$stmt->execute([$cat]);
//$deleted = $stmt->rowCount();