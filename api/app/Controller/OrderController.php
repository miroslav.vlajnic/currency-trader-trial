<?php

namespace App\Controller;

use Core\BaseController;
use App\Model\Order;

class OrderController extends BaseController {

  private $order;

  public function __construct() {
    $this->order = new Order;
  }

  public function calculate($params) {
    var_dump($params);

    $data = $this->order->calculate($params);

    return $this->jsonResponse($data, 200);
  }

}